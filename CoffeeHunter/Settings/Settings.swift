//
//  Settings.swift
//  CoffeeHunter
//
//  Created by Melnik Anatol on 26.07.21.
//

import Foundation
class Settings {
    static let shared = Settings()
    
    private init() {}
    private let defaults = UserDefaults.standard
    
    var useHybridMap: Bool {
        set {
            defaults.set(newValue, forKey: "hybridMap")
        }
        get {
            return defaults.value(forKey: "hybridMap") as? Bool ?? false
        }
    }
}
