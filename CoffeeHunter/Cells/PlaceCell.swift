//
//  PlaceCell.swift
//  CoffeeHunter
//
//  Created by Melnik Anatol on 19.07.21.
//

import UIKit

class PlaceCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var scheduleLabel: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupWith(place: CoffeePlace) {
        nameLabel.text = place.name
        scheduleLabel.text = place.schedule
        adressLabel.text = place.adress
        distanceLabel.text = ""
    }
}
