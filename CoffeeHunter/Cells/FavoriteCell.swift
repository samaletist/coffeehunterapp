//
//  FavoriteCell.swift
//  CoffeeHunter
//
//  Created by Melnik Anatol on 29.07.21.
//

import UIKit

class FavoriteCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var scheludeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupWithFav(place: CoffeePlace) {
        nameLabel.text = place.name
        scheludeLabel.text = place.schedule
        adressLabel.text = place.adress
    }
}
