//
//  PlaceViewController.swift
//  CoffeeHunter
//
//  Created by Melnik Anatol on 13.07.21.
//

import UIKit

class PlaceViewController: UIViewController {
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var scheduleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var socialNetLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    
    var place: CoffeePlace?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissAction))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func dismissAction() {
        dismiss(animated: true)
    }
    
    func setupView() {
        guard let place = place else { return }
        placeNameLabel.text = place.name
        scheduleLabel.text = place.schedule
        descriptionLabel.text = place.descrip
        socialNetLabel.text = place.socialNetwork
    }
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func favoriteAction(_ sender: Any) {
        guard let place = place else { return }
        RealmManager.shared.writeObject(coffeePlace: place)
        let image = UIImage(named: "favicon")
        (sender as AnyObject).setImage(image, for: .normal)
        let alert = UIAlertController(title: "Уведомление", message: "Кофейня добавлена в избранные", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Закрыть" , style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true)
   }
}
