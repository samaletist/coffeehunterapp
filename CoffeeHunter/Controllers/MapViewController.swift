//
//  ViewController.swift
//  CoffeeHunter
//
//  Created by Melnik Anatol on 12.07.21.
//

import UIKit
import GoogleMaps
import CoreLocation

class MapViewController: UIViewController {
    @IBOutlet weak var mapView: GMSMapView!
    
    var places: [CoffeePlace] = Place.init().placeArray
    var place: CoffeePlace?
    var locationManager = CLLocationManager()
    var myLocation: CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.isUserInteractionEnabled = true
        mapView.isMyLocationEnabled = true
        locationManager.delegate = self
        mapView.delegate = self
        locationManager.startUpdatingLocation()
        mapView.isIndoorEnabled = true
        mapView.isBuildingsEnabled = true
        mapView.settings.myLocationButton = true
        mapView.padding = UIEdgeInsets(top: 0, left: 15, bottom: 50, right: 0)
        
        createPin()
    }
    
    func createPin() {
        for place in places {
            let pin = CoffeePin(position: CLLocationCoordinate2D(latitude: place.latPlace,
                                                                 longitude: place.lonPlace))
            pin.map = mapView
            pin.isTappable = true
            pin.place = place
            let view = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
            view.layer.cornerRadius = 15
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            view.addSubview(imageView)
            imageView.center = view.center
            imageView.layer.cornerRadius = 10
            imageView.backgroundColor = .clear
            imageView.image =  UIImage(named: "iconCup")
            view.layer.borderWidth = 1
            view.backgroundColor = .white
            view.layer.borderColor = UIColor.gray.cgColor
            pin.iconView = view
        }
    }
    
    @IBAction func searchAction(_ sender: Any){
    let storyboard = UIStoryboard(name:"Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController else { return  }
        vc.myLocation = myLocation
        vc.placeSelected = { [weak self] place in
            self?.dismiss(animated: true)
            self?.changeCamera(to: CLLocation(latitude: place.latPlace, longitude: place.lonPlace))
            
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            guard let vc = storyboard.instantiateViewController(withIdentifier: "PlaceViewController") as? PlaceViewController else { return }
            vc.view.backgroundColor = .clear
            vc.modalPresentationStyle = .overCurrentContext
            vc.place = place
            
            vc.setupView()
            self?.present(vc, animated: true)
        }
        present(vc, animated: true)
        vc.modalPresentationStyle = .overCurrentContext
    }
    
    @IBAction func settingAction(_ sender: Any) {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "SettingViewController") as? SettingViewController else { return }
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        present(vc, animated: true, completion: nil)
        return
    }
    
    func changeCamera(to coordinates: CLLocation) {
        let camera = GMSCameraPosition(latitude: coordinates.coordinate.latitude, longitude: coordinates.coordinate.longitude, zoom: 15)
        mapView.camera = camera
    }
}

extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let coord = locations.last else { return }
        myLocation = coord
        changeCamera(to: coord)
        locationManager.stopUpdatingLocation()
    }
}

extension MapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "PlaceViewController") as? PlaceViewController else { return false }
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        guard let placePin = marker as? CoffeePin else { return false }
        vc.place = placePin.place
        vc.setupView()
        present(vc, animated: true, completion: nil)
        return true
    }
}

