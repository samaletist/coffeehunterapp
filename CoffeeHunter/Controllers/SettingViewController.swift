//
//  SettingViewController.swift
//  CoffeeHunter
//
//  Created by Melnik Anatol on 19.07.21.
//

import UIKit
import RealmSwift
class SettingViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var savedData: [CoffeePlace] = []
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        registerCell()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        readData()
    }
    
    func readData() {
        savedData = Array(realm.objects(CoffeePlace.self))
        tableView.reloadData()
    }
    
    func registerCell() {
        let nib = UINib(nibName: String(describing: FavoriteCell.self), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: String(describing: FavoriteCell.self))
    }
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true)
    }
}

extension SettingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FavoriteCell.self), for: indexPath)
        guard let placeCell = cell as? FavoriteCell else { return cell }
        placeCell.setupWithFav(place: savedData[indexPath.row] )
        return placeCell
    }
}

extension SettingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let item = savedData[indexPath.row]
        let deleteAction = UIContextualAction(style: .destructive, title: "Удалить") { [self] _, _, complete in
            self.savedData.remove(at: indexPath.row)
            self.tableView.beginUpdates()
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            self.tableView.endUpdates()
            RealmManager.shared.remove(coffeePlace: item)
            complete(true)
        }
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
}


