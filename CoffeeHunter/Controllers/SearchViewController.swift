//
//  SearchViewController.swift
//  CoffeeHunter
//
//  Created by Melnik Anatol on 15.07.21.
//

import UIKit
import CoreLocation

class SearchViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var places: [CoffeePlace] = Place.init().placeArray
    var myLocation: CLLocation?
    var placeSelected: ((CoffeePlace) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        registerCell()
    }
    
    func registerCell() {
        let nib = UINib(nibName: String(describing: PlaceCell.self), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: String(describing: PlaceCell.self))
    }
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true)
    }
    
    func distanceInMeters(place: CoffeePlace) -> Double?  {
        guard let myLoc = myLocation else { return nil}
        let coordinatePlace = CLLocation(latitude: place.latPlace,
                                                     longitude: place.lonPlace)
        return myLoc.distance(from: coordinatePlace)
    }
}

extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        placeSelected?(places[indexPath.row])
    }
}

extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PlaceCell.self), for: indexPath)
        guard let placeCell = cell as? PlaceCell else { return cell }
        placeCell.setupWith(place: places[indexPath.row])
        if let meters = distanceInMeters(place: places[indexPath.row]) {
            placeCell.distanceLabel.text = "\(String(format: "%.2f", meters / 1000)) км."
        } else {
            placeCell.distanceLabel.text = ""
        }
        return placeCell
    }
}

extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let text = searchBar.text else { return }
        if !text.isEmpty {
            places = places.filter({$0.name.lowercased().contains(text.lowercased())})
        } else {
            places = Place.init().placeArray
        }
        tableView.reloadData()
    }
}


