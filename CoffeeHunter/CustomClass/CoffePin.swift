//
//  CoffePin.swift
//  CoffeeHunter
//
//  Created by Melnik Anatol on 13.07.21.
//

import Foundation
import GoogleMaps

class CoffeePin: GMSMarker {
    var place: CoffeePlace?
}
