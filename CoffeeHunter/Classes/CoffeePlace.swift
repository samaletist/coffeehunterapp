//
//  CoffeePlace.swift
//  CoffeeHunter
//
//  Created by Melnik Anatol on 13.07.21.
//

import Foundation
import RealmSwift

class CoffeePlace: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var latPlace: Double = 0.0
    @objc dynamic var lonPlace: Double = 0.0
    @objc dynamic var schedule: String = ""
    @objc dynamic var descrip: String = ""
    @objc dynamic var socialNetwork: String = ""
    @objc dynamic var adress: String = ""
    @objc dynamic var placeId: Int = 0
    
    convenience init(name: String, latPlace: Double, lonPlace: Double, schedule: String, description: String, socialNetwork: String, adress: String, placeId: Int) {
        self.init()
        self.name = name
        self.latPlace = latPlace
        self.lonPlace = lonPlace
        self.schedule = schedule
        self.descrip = description
        self.socialNetwork = socialNetwork
        self.adress = adress
        self.placeId = placeId
    }
}

class Place {
    var placeArray = [CoffeePlace]()
    
    init() {
        let paragraphOne = CoffeePlace(name: "Paragraph", latPlace: 53.909493, lonPlace: 27.575467,schedule: "График работы: 7.30 - 23.00", description: "Место, где можно насладиться чашечкой ароматного кофе, поднять себе настроение чудесной домашней выпечкой, и просто отдохнуть.", socialNetwork: "", adress: "пр-т Независимости, д. 37", placeId: 1)
        placeArray.append(paragraphOne)
        let paragraphTwo = CoffeePlace(name: "Paragraph", latPlace: 53.896315, lonPlace: 27.553611, schedule: "График работы: 7.30 - 23.00", description: "Место, где можно насладиться чашечкой ароматного кофе, поднять себе настроение чудесной домашней выпечкой, и просто отдохнуть.", socialNetwork: "", adress: "ул. Карла Маркса, д. 9", placeId: 2)
        placeArray.append(paragraphTwo)
        let kometa = CoffeePlace(name: "Kometa", latPlace: 53.904745, lonPlace: 27.534370, schedule: "График работы: 8.30 - 18.00", description: "Комета - кофе и конфета", socialNetwork: "", adress: "ул. Короля, д. 51", placeId: 3)
        placeArray.append(kometa)
        let zernoOne = CoffeePlace(name: "Зерно", latPlace: 53.899493, lonPlace: 27.559954, schedule: "График работы: 9.00 - 23.00", description: "Кофейня «Зерно» открыта для всех, кто любит вкусный кофе, хороший китайский чай, фруктовые и ягодные смузи, лимонады, алкогольные и безалкогольные коктейли, домашнюю выпечку и дружелюбную атмосферу.", socialNetwork: "", adress: "ул. Ленина, д. 16", placeId: 4)
        placeArray.append(zernoOne)
        let zernoTwo = CoffeePlace(name: "Зерно", latPlace: 53.912792, lonPlace: 27.580669, schedule: "График работы: 9.00 - 23.00", description: "Кофейня «Зерно» открыта для всех, кто любит вкусный кофе, хороший китайский чай, фруктовые и ягодные смузи, лимонады, алкогольные и безалкогольные коктейли, домашнюю выпечку и дружелюбную атмосферу.", socialNetwork: "", adress: "пр-т Независимости, д. 46", placeId: 5)
        placeArray.append(zernoTwo)
        let cofix = CoffeePlace(name: "Cofix", latPlace: 53.874981, lonPlace: 27.498221, schedule: "График работы: 9.00 - 23.00", description: "Знаменитая сеть кофеен. Качественноая еда и напитки по справедливой цене!", socialNetwork: "inst: Cofix_Bel", adress: "ул. Уманская, д. 54", placeId: 6)
        placeArray.append(cofix)
        let sherloc = CoffeePlace(name: "Sherlock Coffee Hall", latPlace: 53.893885, lonPlace: 27.551240, schedule: "График работы: 8.00 - 23.00", description: "Кофейня Sherlock coffee hall – это таинственно-детективные ощущения, обволакивающее тепло камина и ароматный кофе со свежим десертом. Вас мягко поглотит атмосфера Лондона", socialNetwork: "sherloc_minsk", adress: "пер. Михайловский, д.4", placeId: 7)
        placeArray.append(sherloc)
        let stories = CoffeePlace(name: "Stories", latPlace: 53.900955, lonPlace: 27.552893, schedule: "График работы: 9.00 - 22.00", description: "", socialNetwork: "", adress: "ул. Интернациональная, д. 14", placeId: 8)
        placeArray.append(stories)
        let stihi = CoffeePlace(name: "Стихи, кофе, поцелуи", latPlace: 53.899960, lonPlace: 27.556989, schedule: "График работы: 8.00 - 22.00", description: "Наш кофе вкусный, как первый 💋, А ты уже пробовал нашу бодрящую романтику?😏❤️", socialNetwork: "inst: poems_coffee_kisses", adress: "пр-т Независимости, д. 19", placeId: 9)
        placeArray.append(stihi)
        let inklbaristaOne = CoffeePlace(name: "Инклюзивный бариста", latPlace: 53.902777, lonPlace: 27.547575, schedule: "График работы: 9.00 - 19.00", description: "Место где все сотрудники - это люди с инвалидностью. Управляется человеком с синдромом Дауна. Все бариста колясочники. Место, где вы будете удивлены и поражены мощью инклюзии и приспособленности нашего общества!", socialNetwork: "inst: inclusive_barista", adress: "ул. Романовская Слобода, д. 8", placeId: 10)
        placeArray.append(inklbaristaOne)
        let inklBaristaTwo = CoffeePlace(name: "Инклюзивный бариста", latPlace: 53.904575, lonPlace: 27.561642, schedule: "График работы: 9.00 - 21.00", description: "Место где все сотрудники - это люди с инвалидностью. Управляется человеком с синдромом Дауна. Все бариста колясочники. Место, где вы будете удивлены и поражены мощью инклюзии и приспособленности нашего общества!", socialNetwork: "inst: inclusive_barista", adress: "ул. Интернациональная, д. 36-2", placeId: 11)
        placeArray.append(inklBaristaTwo)
    }
}


