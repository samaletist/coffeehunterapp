//
//  RealmManager.swift
//  CoffeeHunter
//
//  Created by Melnik Anatol on 28.07.21.
//

import Foundation
import RealmSwift

class RealmManager {
    static let shared = RealmManager()
    let realm = try! Realm()

    private init () {}

    func writeObject(coffeePlace: CoffeePlace) {
        try! realm.write {
            realm.add(coffeePlace)
        }
        
    }
    func remove(coffeePlace: CoffeePlace) {
        try! realm.write {
            realm.delete(coffeePlace)
        }
    }
}
